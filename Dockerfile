FROM python:3.10.9

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1



############ New Stage ############


# Install pipenv
RUN pip install --upgrade pip --root-user-action=ignore && \
    pip install -U torchvision torch glob2 opencv-python pymongo numpy


# Install python dependencies in /.venv
COPY . /home


ENTRYPOINT ["tail", "-f", "/dev/null"]
